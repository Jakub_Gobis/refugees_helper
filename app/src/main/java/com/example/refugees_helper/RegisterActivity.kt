package com.example.refugees_helper

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class RegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)


        //Dodanie aktywnosci do przycisku w ekranbie rejestracji - wczytanie danych do zmiennych
        val regRegisterButton: Button = findViewById(R.id.RegRegister_button)
        regRegisterButton.setOnClickListener {
            //Tu należy napisać kod do rejestracji i łączenia z bazą danych
        }
    }
}
