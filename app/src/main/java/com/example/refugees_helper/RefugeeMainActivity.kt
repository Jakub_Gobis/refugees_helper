package com.example.refugees_helper

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ListView

class RefugeeMainActivity : AppCompatActivity() {
    private var i = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_refugee_main)

        val offerBox : EditText = findViewById(R.id.editText_offer)
        val telBox : EditText = findViewById(R.id.editText_phoneNumber)
        val nameBox : EditText = findViewById(R.id.editText_helperName)

        val listView = findViewById<ListView>(R.id.listView)
        val prompts = arrayOfNulls<String>(50)


        val arrayAdapter : ArrayAdapter<String> = ArrayAdapter(
            this, android.R.layout.simple_list_item_1, prompts
        )

        listView.adapter = arrayAdapter

        val addPromptButton : Button = findViewById(R.id.addPrompt_button)
        addPromptButton.setOnClickListener{
            val offer : String = offerBox.text.toString()
            val telNumber : String = telBox.text.toString()
            val name : String = nameBox.text.toString()

            prompts[this.i] = ("$offer ; $telNumber ; $name")
            val nextAdapter : ArrayAdapter<String> = ArrayAdapter(
                this, android.R.layout.simple_list_item_1, prompts
            )
            listView.adapter = nextAdapter
            this.i++
        }
    }
}
