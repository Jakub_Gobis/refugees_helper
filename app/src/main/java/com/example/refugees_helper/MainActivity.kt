package com.example.refugees_helper

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Dodanie aktywności do przycisku w ekranie powitalnym - przejscie do aktywnosci logowania pomagacza
        val helperLoginButton: Button = findViewById(R.id.helper_login_button)
        helperLoginButton.setOnClickListener {
            val intent = Intent(this, HelperLoginActivity::class.java)
            startActivity(intent)
        }

        //Dodanie aktywnosci do przycisku w ekranbie powitalnym - przejscie do aktywnosci logowania uchodzcy
        val refugeeLoginButton: Button = findViewById(R.id.refugee_login_button)
        refugeeLoginButton.setOnClickListener {
            val intent = Intent(this, RefugeeLoginActivity::class.java)
            startActivity(intent)
        }

        //Dodanie aktywnosci do przycisku w ekranbie powitalnym - przejscie do aktywnosci rejestracji profilu
        val registerButton: Button = findViewById(R.id.register_button)
        registerButton.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }
}
