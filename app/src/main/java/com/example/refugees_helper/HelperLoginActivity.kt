package com.example.refugees_helper

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.*

class HelperLoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_helper_login)

        val loginBox: EditText = findViewById(R.id.textBox_login)
        val passwordBox: EditText = findViewById(R.id.textBox_password)
        val incorrectPassword : TextView = findViewById(R.id.insertIncorrect_textView)

        val adminUsername = "admin"
        val adminPassword = "admin"

        val helperLoginButton: Button = findViewById(R.id.login_button)
        helperLoginButton.setOnClickListener {

            val writtenUsername : String = loginBox.text.toString()
            val writtenPassword : String = passwordBox.text.toString()

            if((writtenPassword == adminPassword) && (writtenUsername == adminUsername))
            {
                val intent = Intent(this, HelperMainActivity::class.java)
                startActivity(intent)
            }
            else
            {
                incorrectPassword.text = "Wprowadź poprawne dane logowania!"
            }
        }
    }
}